import asyncio
import struct
from bleak import BleakClient, BleakScanner, BleakError
from datetime import datetime

def decode_string(data):
    # Définir le format de la structure
    format_string = '8s I I Q'

    # Analyser la chaîne en octets selon le format spécifié
    decoded_data = struct.unpack(format_string, data)

    # Extraire les éléments décodés
    name = decoded_data[0].decode().rstrip('\x00')
    size = decoded_data[1]
    hash_value = decoded_data[2]
    creation_date = decoded_data[3]

    # Retourner les données décodées
    return {'name': name, 'size': size, 'hash': hash_value, 'creation_date': creation_date}

async def get_device_by_address(s):
    devices = await BleakScanner.discover()
    for d in devices:
        if d.address == s:
            return d
        
def notification_callback(sender: int, data: bytearray):
    if sender == 111:
        print(f"Notification received from LOG: {data}")
    if sender == 58:
        decoded_data = decode_string(data)
        print(f"Notification received from LIST: {decoded_data}")
    if sender == 66:
        print(f"Notification received from READ: {data}")
    if sender == 100:
        print(f"Notification received from ACK: {data}")
        
    

address = "E7:90:A2:C0:1A:29"
service_uuid = "1b0d1303-a720-f7e9-46b6-31b601c4fca1"

async def run():
    device = await get_device_by_address(address)
    print(device.details)
    try:
        async with BleakClient(device.address) as client:
            if client.is_connected:
                print("Connected")
                services = await client.get_services()
                print("Services disponibles sur OBU-6771: ")
                for service in services:
                    print(service)                
                    for charac in service.characteristics:
                        if (charac.uuid in ["1b0d1303-a720-f7e9-46b6-31b601c4fca1","1b0d1407-a720-f7e9-46b6-31b601c4fca1","1b0d1302-a720-f7e9-46b6-31b601c4fca1", "1b0d140a-a720-f7e9-46b6-31b601c4fca1","1b0d1304-a720-f7e9-46b6-31b601c4fca1"]):
                                print(charac)
                                try:
                                    data = await client.read_gatt_char(charac)
                                    print("Read data:", data)
                                except BleakError as e:
                                    print(f"Error reading characteristic {charac.uuid}: {e}")
                                await client.start_notify(charac.uuid, notification_callback)
                n = await client.read_gatt_char("1b0d1303-a720-f7e9-46b6-31b601c4fca1")
                await client.write_gatt_char("1b0d1302-a720-f7e9-46b6-31b601c4fca1", n)
                for x in FileArray:
                    await client.write_gatt_char("1b0d1304-a720-f7e9-46b6-31b601c4fca1",x)             
                await asyncio.sleep(10)
                await client.stop_notify(charac.uuid)
                
    except Exception as e:
        print(e)
        
class DataStructure:
    def __init__(self):
        self.name = b''
        self.chunk_id = 0
        self.fin_chunk_id = 9999

    def set_from_byte_array(self, byte_array):
        if len(byte_array) >= 16:  # Vérifier si la longueur du tableau d'octets est suffisante
            # Déballer les données du tableau d'octets en utilisant le format spécifié
            self.name, self.chunk_id, self.fin_chunk_id = struct.unpack('8s I I', byte_array[:16])
        else:
            raise ValueError("Byte array is too short to parse")

    def to_byte_array(self):
        # Emballer les données de la structure dans un tableau d'octets selon le format spécifié
        return struct.pack('8s I I', self.name, self.chunk_id, self.fin_chunk_id)

# Exemple d'utilisation
structureT10 = DataStructure()
structureT10.name = b'T10'
byte_arrayT10 = structureT10.to_byte_array()

structureT11 = DataStructure()
structureT11.name = b'T11'
byte_arrayT11 = structureT11.to_byte_array()

structureT13 = DataStructure()
structureT13.name = b'T13'
byte_arrayT13 = structureT13.to_byte_array()

structureT6771 = DataStructure()
structureT6771.name = b'T6771'
byte_arrayT6771 = structureT6771.to_byte_array()

FileArray = [byte_arrayT10, byte_arrayT11, byte_arrayT13, byte_arrayT6771]

asyncio.run(run())
