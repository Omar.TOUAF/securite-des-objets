import asyncio
from bleak import BleakScanner

async def main():
    devices = await BleakScanner.discover(timeout = 10.0)
    for d in devices:
        #if d.name and d.name.startswith("O"):
        print(d)

asyncio.run(main())